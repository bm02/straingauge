/*----- PROTECTED REGION ID(StrainGauge.h) ENABLED START -----*/
//=============================================================================
//
// file :        StrainGauge.h
//
// description : Include file for the StrainGauge class
//
// project :     StrainGauge
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2018
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef StrainGauge_H
#define StrainGauge_H

#include <tango.h>
#include <Serial.h>


/*----- PROTECTED REGION END -----*/	//	StrainGauge.h

/**
 *  StrainGauge class description:
 *    StrainGauges used at BM02
 */

namespace StrainGauge_ns
{
/*----- PROTECTED REGION ID(StrainGauge::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations

/*----- PROTECTED REGION END -----*/	//	StrainGauge::Additional Class Declarations

class StrainGauge : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(StrainGauge::Data Members) ENABLED START -----*/

//	Add your own data members
    Tango::DeviceProxy * serial;
    bool device_constructed;

/*----- PROTECTED REGION END -----*/	//	StrainGauge::Data Members

//	Device property data members
public:
	//	Serialline:	Name of a tango device server, which the device is connected to.
	string	serialline;

	bool	mandatoryNotDefined;

//	Attribute data members
public:
	Tango::DevDouble	*attr_Position_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	StrainGauge(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	StrainGauge(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	StrainGauge(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~StrainGauge() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();

	/*
	 *	Check if mandatory property has been set
	 */
	 void check_mandatory_property(Tango::DbDatum &class_prop, Tango::DbDatum &dev_prop);

//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : StrainGauge::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute Position related methods
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_Position(Tango::Attribute &attr);
	virtual bool is_Position_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : StrainGauge::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:


	//--------------------------------------------------------
	/**
	 *	Method      : StrainGauge::add_dynamic_commands()
	 *	Description : Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(StrainGauge::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes

/*----- PROTECTED REGION END -----*/	//	StrainGauge::Additional Method prototypes
};

/*----- PROTECTED REGION ID(StrainGauge::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	StrainGauge::Additional Classes Definitions

}	//	End of namespace

#endif   //	StrainGauge_H
